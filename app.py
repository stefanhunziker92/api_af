from datetime import datetime
import random
import string
import jwt
import boto3
from werkzeug.exceptions import Unauthorized
import requests
import json
import flask
from flask import Flask, request, Response, make_response, jsonify
from flask import send_from_directory
from flask import send_file
from flask_cors import CORS
from flask_restplus import Api, Resource, inputs, fields, marshal
import werkzeug
from base64 import b64encode
werkzeug.cached_property = werkzeug.utils.cached_property


# config
app = flask.Flask(__name__)
app.config["PROPAGATE_EXCEPTIONS"] = False
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'
api = Api(app, version='1.0', title='ares_Api')


# parser for ares-login
parser = api.parser()
parser.add_argument('token', type=str, required=False,
                    help='Bearer Access Token.')

parser.add_argument(
    "username", type=str, required=True, help="username")

parser.add_argument(
    "password", type=str, required=True, help="password")

# parser for ares-send-message
parser_ares_send_message = api.parser()

parser_ares_send_message.add_argument('token', type=str, required=False,
                                      help='Bearer Access Token.')

parser_ares_send_message.add_argument(
    "JSON", type=str, required=False, help="JSON string")

# parser for add_notes
parser_ares_add_notes = api.parser()

parser_ares_add_notes.add_argument('token', type=str, required=False,
                                      help='Bearer Access Token.')

parser_ares_add_notes.add_argument(
    "json", type=str, required=False, help="json")

# parser for get-megssage-ares
parser_ares_get_message = api.parser()
parser_ares_get_message.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_ares_get_message.add_argument(
    "sess_id", type=str, required=True, help="SessionID")

# define for auth
parser_credentials = api.parser()

parser_credentials.add_argument(
    "username", type=str, required=True, help="username")

parser_credentials.add_argument(
    "password", type=str, required=True, help="password")

CORS(app, origins=r'*', allow_headers=r'*')

# parser for change_note
parser_ares_change_note = api.parser()
parser_ares_change_note.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_ares_change_note.add_argument(
    "sess_id", type=str, required=True, help="SessionID")

parser_ares_change_note.add_argument(
    "note_id", type=str, required=True, help="Note ID")

parser_ares_change_note.add_argument(
    "note", type=str, required=True, help="Note to be saved")

# parser for delete_note
parser_ares_delete_note = api.parser()
parser_ares_delete_note.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_ares_delete_note.add_argument(
    "sess_id", type=str, required=True, help="SessionID")

parser_ares_delete_note.add_argument(
    "note_id", type=str, required=True, help="Note ID")

# parser for add important note
parser_ares_add_imp_note = api.parser()
parser_ares_add_imp_note.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_ares_add_imp_note.add_argument(
    "sess_id", type=str, required=True, help="SessionID")

parser_ares_add_imp_note.add_argument(
    "dialog_id", type=str, required=True, help="Note ID")
parser_ares_add_imp_note.add_argument(
    "text", type=str, required=True, help="Note to be added")

# parser for add important note
parser_ares_accept_friends = api.parser()
parser_ares_accept_friends.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_ares_accept_friends.add_argument(
    "accept_id", type=str, required=True, help="ID for Friendrequest")

# parser for statistics
parser_ares_get_stats = api.parser()
parser_ares_get_stats.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_ares_get_stats.add_argument(
    "mod_id", type=str, required=False, help="morderator_profile.id")


# models
token = fields.String(example='"eyJraWQiOiI5M2NHeEY5dHhINTl2SWJpVDR1NDZQQ0VvOFNGZ2xEdno5THg1UVRcL2k3Yz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJjOWRmNTUzMC0zOGQ3LTQzMTEtODM5Zi00OThmNTMzZDU0ZmYiLCJjb2duaXRvOmdyb3VwcyI6WyJiYXNpY3BsYW4iXSwiZXZlbnRfaWQiOiI5MzA2Yjk3NC1hZWE3LTQ4MGYtYjY4Yi1kZDhjZTkzMzhmZmMiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIiwiYXV0aF90aW1lIjoxNjAyMDY1NDEzLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV9uZTYwczlEUXkiLCJleHAiOjE2MDIwNjkwMTMsImlhdCI6MTYwMjA2NTQxMywianRpIjoiN2QxYjI4YmYtYmIxNS00MzcyLWFiYjktN2ExZmRmMDUzOWM2IiwiY2xpZW50X2lkIjoiM3JzanRidHNmMmJ2NWYxODdkOTk4OWQyc2wiLCJ1c2VybmFtZSI6ImM5ZGY1NTMwLTM4ZDctNDMxMS04MzlmLTQ5OGY1MzNkNTRmZiJ9.RS-tvI0-Fdb8gvJAxvr2jVSs3ChhPAlXEGGkJu2EoA28PgVdKFfqui9IRzHtGVZi4CDpRs8ZAsA7g7FApF5bJmONnWeu2WWpghMsuEZFixiaEfVlCajhukMZZZC_KiAyxCdKGwti5_DbvWpmGBwdqdlQOWOLhZFPCm2XgnxFp_3ANrqZEJhXwGDTekxl-AMyb1L1s-msE6-5EgVqv9ZggTg9eEwNFc_VfKw87ahDP8sK9Qgz6ZPTsozhCo9vXMQ-YfAlAAicIiK-nJoSwo3fhjLMF-rgcFPySDS9ePWKb08vJfyWLp3-itZHwmn6d5ULKJkE0Q8e-5SSUNfUIX1dxQ"')

# login für ares


@api.route('/ares-login', doc={"description": "Login at ares with ares credentials"})
class Loginares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser)
    def post(self):
        args = parser.parse_args()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # Log into the chat
        isLogged = login_ares(args["username"], args["password"])

        return isLogged

# login ares parameter l for login-id and p for password


def login_ares(l_id, psswd):

    url = "https://mod-panel.anonyme-flirts.net/login"

    payload = {'email': l_id,
               'password': psswd,
               '_token': ''
               }
    files = [
    ]
    headers = {
        'Cookie': 'anonyme_flirtsnet_session='
    }

    response = requests.request(
        "POST", url, headers=headers, data=payload, files=files)

    if 'loginInputPassword' not in response.text:
        cj = response.cookies
        cj = {
            'sess_id': cj['anonyme_flirtsnet_session']
        }
        return cj
    else:
        raise ValueError("Error, please check credentials")


@api.route('/ares-get-message', doc={"description": "check for new messages"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_ares_get_message)
    def post(self):
        args = parser_ares_get_message.parse_args()
        global chat_obj
        chat_obj = []

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get chats
        rslt = check_message_ares(args["sess_id"])
        return rslt

# get message


def check_message_ares(sess_id):

    url = "https://mod-panel.anonyme-flirts.net/api/v2/moderation/"

    payload = {}
    files = {}
    headers = {
        'authority': 'mod-panel.anonyme-flirts.net',
        'method': 'GET',
        'path': '/api/v2/moderation/',
        'scheme': 'https',
        'accept': 'application/json, text/plain, */*',
        'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
        'referer': 'https://mod-panel.anonyme-flirts.net/chat',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36',
        'Cookie': 'anonyme_flirtsnet_session={}'.format(sess_id)
    }

    response = requests.request(
        "GET", url, headers=headers, data=payload, files=files)

    if "Leider ist kein Dialog" in response.text:
        raise ValueError("Currently no conversation available")
    else:
        chat_obj.append(json.loads(response.text))
        jsn = json.loads(response.text)
        c_id = jsn['customer']['id']
        print(c_id)
        f_id = jsn['fake']['id']
        print(f_id)
        return get_message_ares(sess_id, c_id, f_id)


def get_message_ares(sess_id, customer_id, fake_id):
    url = "https://mod-panel.anonyme-flirts.net/api/v1/dialog/messages/{}/{}?skip=0".format(
        customer_id, fake_id)
    print(url)

    payload = {'skip': ' 0'}
    files = [
    ]
    headers = {
        'authority': 'mod-panel.anonyme-flirts.net',
        'method': 'GET',
        'path': '/api/v1/dialog/messages/{}/{}?skip=0'.format(customer_id, fake_id),
        'scheme': 'https',
        'accept': 'application/json, text/plain, */*',
        'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
        'referer': 'https://mod-panel.anonyme-flirts.net/chat',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36',
        'Cookie': 'anonyme_flirtsnet_session={}'.format(sess_id)
    }

    response = requests.request(
        "GET", url, headers=headers, data=payload, files=files)

    chat_obj.append(json.loads(response.text))
    return chat_obj


# senden für ares
@api.route('/ares-send-message', doc={"description": "send message"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_ares_send_message)
    def post(self):

        args = parser_ares_send_message.parse_args()
        args_body = request.get_json()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = ares_send_message(args_body)


# send message
def ares_send_message(args):

    url = "https://mod-panel.anonyme-flirts.net/api/v1/message/all"

    obj = {"messages": [{"type": None, "meta": None, "body": args['message'],
                             "message": args['message'],
                             "receiver_id": args['customerID'], "sender_id": args['fakeID'], "delta": 0}]}

    payload = json.dumps(obj)
    headers = {
        'authority': 'mod-panel.anonyme-flirts.net',
        'method': 'POST',
        'path': '/api/v1/message/all',
        'scheme': 'https',
        'accept': 'application/json, text/plain, */*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
        'content-type': 'application/json;charset=UTF-8',
        'cookie': 'anonyme_flirtsnet_session={};'.format(args['key']['sess_id']),
        'origin': 'https://mod-panel.anonyme-flirts.net',
        'referer': 'https://mod-panel.anonyme-flirts.net/chat',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
        'sec-ch-ua-mobile': '?0',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    return response

# notes für ares
@api.route('/ares-add-notes', doc={"description": "send message"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_ares_add_notes)
    def post(self):

        args = parser_ares_add_notes.parse_args()
        args_body = request.get_json()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = ares_add_notes(args_body)


# ares add notes (customer/fake notes)
def ares_add_notes(args):

    url = "https://mod-panel.anonyme-flirts.net/api/v1/dialog/notes/{}".format(args['dialogID'])
    print(url)

    if args['sex'] == 'male':
        payload = {
            'customer_notes': args['notes'],
        }
    else:
        payload = {
            'fake_notes': args['notes'],
        }

    payload = json.dumps(payload)
    headers = {
        'authority': 'mod-panel.anonyme-flirts.net',
        'method': 'PATCH',
        'path': '/api/v1/message/all',
        'scheme': 'https',
        'accept': 'application/json, text/plain, */*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
        'content-type': 'application/json;charset=UTF-8',
        'cookie': 'anonyme_flirtsnet_session={};'.format(args['key']['sess_id']),
        'origin': 'https://mod-panel.anonyme-flirts.net',
        'referer': 'https://mod-panel.anonyme-flirts.net/chat',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
        'sec-ch-ua-mobile': '?0',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin'
    }

    response = requests.request("PATCH", url, headers=headers, data=payload)
    return response



# notes für ares
@api.route('/ares-change-note', doc={"description": "change important note"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_ares_change_note)
    def post(self):

        args = request.get_json()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = ares_change_note(args)
        return rslt
        

# ares add notes (important notes)
def ares_change_note(args):

    url = "https://mod-panel.anonyme-flirts.net/api/v1/dialogInfoMessage/{}".format(args['note_id'])
 
    payload = json.dumps({
    "text": args['note']
    })
    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'anonyme_flirtsnet_session={};'.format(args['key']['sess_id']),
    }

    response = requests.request("PATCH", url, headers=headers, data=payload)
    return response.text


# notes für ares
@api.route('/ares-delete-note', doc={"description": "change important note"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_ares_delete_note)
    def post(self):

        args = request.get_json()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = ares_delete_note(args)
        return rslt
        

# ares add notes (important notes)
def ares_delete_note(args):

    url = "https://mod-panel.anonyme-flirts.net/api/v1/dialogInfoMessage/{}".format(args['note_id'])

    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'anonyme_flirtsnet_session={};'.format(args['key']['sess_id']),
    }

    response = requests.request("DELETE", url, headers=headers)
    return response.text

# notes für ares
@api.route('/ares-add-imp-note', doc={"description": "change important note"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_ares_add_imp_note)
    def post(self):

        args = request.get_json()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = ares_add_imp_note(args)
        return rslt
        

# ares add notes (important notes)
def ares_add_imp_note(args):

    url = "https://mod-panel.anonyme-flirts.net/api/v1/dialogInfoMessage"

    payload = json.dumps({
    "text": args['text'],
    "dialog_id": args['dialog_id']
    })
    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'anonyme_flirtsnet_session={};'.format(args['key']['sess_id']),
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    return response.text


# accept friends ares
@api.route('/ares-accept-friend', doc={"description": "change important note"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_ares_add_imp_note)
    def post(self):

        args = request.get_json()
        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = ares_accept_friend(args)
        return rslt
        

# ares add notes (important notes)
def ares_accept_friend(args):

    url = "https://mod-panel.anonyme-flirts.net/api/v1/friends/{}/accept".format(args['accept_id'])

    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'anonyme_flirtsnet_session={};'.format(args['key']['sess_id']),
    }

    response = requests.request("GET", url, headers=headers)
    return response.text



# accept get user stats
@api.route('/ares-get-stats', doc={"description": "get statistics"})
class getMessagediez(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_ares_get_stats)
    def post(self):

        args = request.get_json()
        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = ares_get_stats(args)
        return rslt
        
# ares add notes (important notes)
def ares_get_stats(args):

    url = "https://mod-panel.anonyme-flirts.net/api/v1/moderator-transaction/history/{}?filter[]=%7B%22name%22:%22sort%22,%22mode%22:%22DESC%22,%22attribute%22:%22created_at%22%7D&filter[]=%7B%22name%22:%22count%22%7D&tableId=table-2&fetchOnQueue=false".format(args['mod_id'])

    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'anonyme_flirtsnet_session={};'.format(args['key']['sess_id']),
    }

    response = requests.request("GET", url, headers=headers)

    return sort_numbers(response.text)


def sort_numbers(rsp):
    jsn = json.loads(rsp)
    monthly = jsn['this month'].split(' / ')
    today = jsn['today'].split(' / ')
    rslt = { 
        "today": today,
        "monthly": monthly
    }
    return rslt



# auth
@api.route("/oauth/token", doc={
    "description": "Returns a Bearer authentication token. The token is valid for 30 minutes."
})
class awsAuth(Resource):
    @api.expect(parser_credentials)
    @api.response(200, 'Success', token)
    @api.response(401, 'Unauthorized Error')
    def get(self):
        args = parser_credentials.parse_args()
        username = args["username"]
        password = args["password"]
        try:
            client = boto3.client("cognito-idp", "eu-central-1")
            response = client.initiate_auth(
                AuthFlow="USER_PASSWORD_AUTH",
                AuthParameters={"USERNAME": username, "PASSWORD": password, },
                ClientId="41ft5d2kpm37ctefvbf0pb3m29",
                ClientMetadata={"UserPoolId": "eu-central-1_o8ZrmNcQP"},
            )
        except:
            raise Unauthorized('That email/password combination is not valid.')

        Token = response["AuthenticationResult"]["AccessToken"]
        return Token


# get user
def get_user_name(token):
    region = "eu-central-1"
    provider_client = boto3.client("cognito-idp", region_name=region)
    try:
        user = provider_client.get_user(AccessToken=token)
    except provider_client.exceptions.NotAuthorizedException as e:
        raise Unauthorized('Invalid token!')
    return user["Username"]


@ api.errorhandler(ValueError)
def handle_value_exception(error):
    err_message = str(error)
    return {'message': err_message}, 400


@api.representation('image/jpeg')
def img_response(data, code, headers=None):
    response = app.make_response(data)
    response.headers['Content-Type'] = 'image/jpeg'
    return response


if __name__ == "__main__":
    app.run()
